import { Numenv1Page } from './app.po';

describe('numenv1 App', function() {
  let page: Numenv1Page;

  beforeEach(() => {
    page = new Numenv1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
