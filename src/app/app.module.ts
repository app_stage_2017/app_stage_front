import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AppRoutingModule} from 'app/shared/app-routing/app-routing.module';
import { Ng2CompleterModule } from "ng2-completer";
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';

import {CurrentProjectsModule} from 'app/user/current-projects/current-projects.module';
import {HomeModule} from 'app/shared/home/home.module';
import {FinishProjectsModule} from 'app/user/finish-projects/finish-projects.module';
import { CreateProjectModule } from 'app/user/create-project/create-project.module';
import {AdminModule} from 'app/admin/admin.module';

import {AuthGuard} from 'app/shared/service/authguard.service';
import {AuthGuardAdmin} from 'app/shared/service/authguardadmin.service';
import {ConnectServer} from 'app/shared/service/connectserver.service';





@NgModule({
  declarations: [
    AppComponent,

   
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CurrentProjectsModule,
    AppRoutingModule,
    FinishProjectsModule,
    HomeModule,
    CreateProjectModule,
    AdminModule,
    Ng2CompleterModule
  //  NgbModule.forRoot(),
    
  ],
  providers: [AuthGuard, ConnectServer,AuthGuardAdmin],
  bootstrap: [AppComponent]
})
export class AppModule { }
