import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'num-rec-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private userName : string;
  private userType : string;

 ngOnInit() { 
   this.userName = localStorage.getItem('name');
   this.userType = localStorage.getItem('type');
  }
 

}
