import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
 
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router) { }
 
    canActivate() {
        if (localStorage.getItem('id') && localStorage.getItem('type')!='Admin') {
            // logged in so return true
            return true;
        }
 
        else {
            this.router.navigate(['/home']);
            return false;
        }
        
    }
}