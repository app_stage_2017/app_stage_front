import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestMethod,RequestOptionsArgs,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';   
import {ConnectServer} from'app/shared/service/connectserver.service'; 

import {Project,Categorie,Categories,Task,Log} from'app/shared/model/project';

@Injectable()
export class FileService {
url : String;

constructor(private _http: Http, private _connect : ConnectServer){
    this.url = this._connect.connect();
}

 createFile(formData : FormData) : Observable <any> 
 {
    return this._http.post(this.url + "/files", formData)
                        .map(files => files.json());;
 }

}