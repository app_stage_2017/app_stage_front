import { Injectable } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {ConnectServer} from'app/shared/service/connectserver.service';


@Injectable()
export class AuthService {
  private _headers = new Headers({'Content-Type': 'application/json'});

url : String;
 

  constructor(private http: Http, private _connect: ConnectServer){
    this.url=this._connect.connect();

  }

   public login(login : string, password: string) : Observable<any> 
   {
      return  this.http.post(this.url + '/login',
                            {login:login, password: password},
                           {headers: this._headers}
                         )
   .map((res : Response) => 
      {
        if(res.status == 200) 
        {
         return res.json();
       }
       else  
       {
         return false;
       }
      });
   }


 
  public logout():void
  {
        localStorage.removeItem('login');
        localStorage.removeItem('id');
        localStorage.removeItem('name');
        localStorage.removeItem('lastName');
        localStorage.removeItem('trigramme');
        localStorage.removeItem('type');
        localStorage.removeItem('role');

  }


}