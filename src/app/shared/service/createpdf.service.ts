import { Injectable } from '@angular/core';
import {Project,Categorie,Categories,Task,Log} from'app/shared/model/project';
//let jsPDF = require('jspdf'); //MAC
declare let jsPDF;  // WIN

@Injectable()
export class CreatePdfService {

createPDF(project : Project)
{
  let doc = new jsPDF();
  let date = new Date().toLocaleDateString();
 
  doc.setFontSize(16);
  doc.text(100,10, "Livre de recettage", null, null, 'center');
  doc.text(100,20, date, null, null, 'center');
  doc.text(100,30,"Projet: " + project.name, null, null, 'center');
  doc.text(100,40,"Client: " + project.client, null, null, 'center');
  doc.text(100,50,"Model: "+project.model, null, null, 'center');

 // console.log(this.selectProject.categorie.categories);
let pageHeight= doc.internal.pageSize.height;
 let rows = [];


    let i=60;
   
    //let category: number;

        for(let category in project.categorie.categories){
          if(i >= pageHeight)
          {
            doc.addPage();
            i = 10;
          }
            doc.setFontSize(12);
            let j =0;
          //  doc.setFillColor(40,20,80,20);
           doc.text(20, i,project.categorie.categories[category].categoryName);
           
               
               let col= ["nom tache","etat","commentaire","date","user"];
               let rows = [];
                 for(let task in project.categorie.categories[category].tasks)
                 {
                  let rows2 = [];
                  rows2.push(project.categorie.categories[category].tasks[task].name);
                  if(project.categorie.categories[category].tasks[task].statut == 'sans')
                  {
                    rows2.push("");
                  }
                  else
                  { 
                  rows2.push(project.categorie.categories[category].tasks[task].statut);
                  }
                  rows2.push(project.categorie.categories[category].tasks[task].commentary);
                  rows2.push(project.categorie.categories[category].tasks[task].endDate);
                  if( typeof(project.categorie.categories[category].tasks[task].logHistory[0]) != 'undefined')
                  {
                  rows2.push(project.categorie.categories[category].tasks[task].logHistory[0].userTrigramme);
                  }
                  else
                  {
                  rows2.push("");
                  }
                  rows.push(rows2);
                  j++;                 
                 //   j+=10;
                                     
                 }
           //    console.log(rows); 
               let dep = i + 5;
              doc.autoTable(col, rows,{startY: dep});
                 
           i=i+20+(j*10);
         //  console.log(i);
        }
       
    let doctitle : string = project.name +'.pdf';
    doc.save(doctitle);
    
}

}    