import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Client} from 'app/shared/model/client';
import 'rxjs/add/operator/map'; 
import {ConnectServer} from'app/shared/service/connectserver.service';  

@Injectable()
export class ClientService {
url : String;
  constructor(private _http: Http, private _connect : ConnectServer){
    this.url = this._connect.connect();
  }


  getClientFromAPI() : Observable <any>{
        return this._http.get(this.url + '/clients')
               // .do(x=>console.log(x))
                .map(clients => clients.json());                       

   }

   deletClientFrompAPI(idClient : number) : Observable <any> {
       return this._http.delete(this.url+'/clients/'+ idClient);
   }

   createClient(clientName : string, clientCode: string){
    return this._http.post(this.url+'/clients', 
     {
       clientName: clientName,
       clientCode : clientCode,
       
      
      })
       .map(clients => clients.json());
   }

}