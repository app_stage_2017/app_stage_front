import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestMethod,RequestOptionsArgs,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/do'; 
import 'rxjs/add/operator/toPromise'; 
import {ConnectServer} from'app/shared/service/connectserver.service'; 

import {Project,Categorie,Categories,Task,Log} from'app/shared/model/project';

@Injectable()
export class LogService {
url : String;

constructor(private _http: Http, private _connect : ConnectServer){
    this.url = this._connect.connect();
}


    getLogByIdTask(taskId : string): Observable<any>{
     return this._http.get(this.url+"/logs/"+taskId)
            .map(logs=>logs.json());
   }

   createLogByIdTask(taskId : string, userTrigramme : string, date : Date, typeLog : string, commentary : string) : Observable <any>{
       return this._http.post(this.url + "/logs", 
       {
           fktask : taskId,
           date : date,
           userTrigramme : userTrigramme,
           typeLog : typeLog,
           commentary : commentary

       });

   }

}   