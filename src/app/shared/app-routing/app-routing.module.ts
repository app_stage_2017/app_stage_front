import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {CurrentProjectsComponent} from'app/user/current-projects/current-projects.component';
import {HomeComponent} from 'app/shared/home/home.component';
import {FinishProjectsComponent} from 'app/user/finish-projects/finish-projects.component';
import {CreateProjectComponent} from 'app/user/create-project/create-project.component';
import { AffectcategoryComponent } from 'app/user/create-project/affectcategory/affectcategory.component';
import { UsermanagerComponent } from 'app/admin/usermanager/usermanager.component';
import { ModelmanagerComponent } from 'app/admin/modelmanager/modelmanager.component';
import {ProjectmanagerComponent} from 'app/admin/projectmanager/projectmanager.component';
import { ClientmanagerComponent } from 'app/admin/clientmanager/clientmanager.component';

import {AuthGuard} from 'app/shared/service/authguard.service';
import {AuthGuardAdmin} from 'app/shared/service/authguardadmin.service';

const routes : Routes = [
  {path:'',redirectTo: '/home', pathMatch:'full'},
  {path:'home', component : HomeComponent},
  {path: 'currentproject', component : CurrentProjectsComponent , canActivate: [AuthGuard]},
  {path:'finishproject', component : FinishProjectsComponent,canActivate: [AuthGuard] },
  {path:'createproject', component:CreateProjectComponent,canActivate: [AuthGuard] },
  {path:'projectmanager', component: ProjectmanagerComponent,canActivate: [AuthGuardAdmin]},
  {path:'usermanager', component: UsermanagerComponent,canActivate: [AuthGuardAdmin]},
  {path:'modelmanager', component: ModelmanagerComponent,canActivate: [AuthGuardAdmin]},
  {path:'clientmanager', component: ClientmanagerComponent,canActivate: [AuthGuardAdmin]}
]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
