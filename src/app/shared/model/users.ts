export class User {
    id?: number;
    userName?: string;
    lastName?: string;
    userTrigramme?: string;
    login?:string;
    password?:string;
    creationDate?: Date;
    userType?:string;
   // role?: string;
}