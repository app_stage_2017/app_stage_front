export class Project {
    id?: number;
    name?:string;
    client?:Client;
    model?:string;
    startDate?: Date;
    endDate?:Date;
    state?:string;
    creator?:string;
    categorie?: Categorie;
}

export class Client
{
    id?: number;
    clientName : string;
    clientCode: string;
    
}

export class Categorie {
        categories: Categories[];
    }

export class Categories {
    categoryName?:string;
    tasks?:Task[];

}   

export class Task {
      //  id?: number;
        id?: string;
        name?: string;
        description?: string;
        statut?: string;
        commentary?: string;
        fichier_joint?: string;
        category?:string;
        creationDate?:Date;
        endDate?:Date;
        user?: string;
        date?: Date;
        fkproject?: number;
        userType?:string;
        commentaire?: string;
        logHistory?:Log[];
        attachedFile?:AttachedFile[];
    }  

export class Log {
    id?:string;
    fktask?:number;
    date?:Date;
    userTrigramme?:string;
    typeLog?:string;
    commentary?:string;
}  

export class AttachedFile 
{
    id?:string;
    namefile?:string;
    namepath?:string;
}   