export class Categorie {
    id?:number;
    name?: string ;
    newCategory?:boolean;
    task? : Task [];
}

export class Task {
    id?: number;
    name?:string;
    description?:string;
    categorie?:string;
    userType?:string;
    newTask?:boolean;
    modelAttachedFile?: ModelAttachedFile[];
}

export class ModelAttachedFile{
    id?:number;
    namefile?:string;
    namepath?: string;


}