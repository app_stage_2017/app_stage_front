import { Component, Input, Output, EventEmitter } from '@angular/core';
import {User} from 'app/shared/model/users';
import {UserService } from 'app/shared/service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-deleteuser',
  templateUrl: './deleteuser.component.html',
  styleUrls: ['./deleteuser.component.css']
})
export class DeleteuserComponent  {
@Input()
viewUserManager : boolean;
@Input()
viewDeleteUser : boolean;
@Input()
users :User[];

@Output() 
viewDeleteUserFalse : EventEmitter<any> = new EventEmitter();
@Output() 
viewUserManagerTrue : EventEmitter<any> = new EventEmitter();

userSelectName : string ;

  constructor(private userservice : UserService, private router : Router) { }

deleteUser(){
  let userSelectId : number;
  for (let user of this.users)
  {
    if(user.userName == this.userSelectName)
    {
      userSelectId = user.id;
    }
  }
 // console.log(userSelectId);
  this.userservice .deletUserFrompAPI(userSelectId).subscribe();
  this.viewUserManager = true;
  this.viewDeleteUser = false;
  this.viewDeleteUserFalse.emit(this.viewDeleteUser);
  this.viewUserManagerTrue .emit(this.viewUserManager);
  this.router.navigate(["/projectmanager"]); 


}

}
