import { Component, Input, Output, EventEmitter} from '@angular/core';
import {User} from 'app/shared/model/users';
import {UserService} from 'app/shared/service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.css']
})
export class CreateuserComponent  {
@Input()
viewUserManager : boolean;
@Input()
viewCreateUser : boolean;

private user: User;

  constructor(private userservice : UserService, private router : Router) { 
    this.user = new User();
  }


public onCreate(value : User){
// console.log(value.userName);
// console.log(value.userType);
 let newDate = new Date()  ;

 this.userservice.createUser(value.userName,value.lastName,value.userTrigramme,value.login,value.password,newDate,value.userType).subscribe();
  this.router.navigate(["/projectmanager"]); 
}


}
