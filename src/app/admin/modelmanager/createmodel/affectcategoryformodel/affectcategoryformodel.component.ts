import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import {Categorie, Task} from 'app/shared/model/categorie';


@Component({
  selector: 'num-rec-affectcategoryformodel',
  templateUrl: './affectcategoryformodel.component.html',
  styleUrls: ['./affectcategoryformodel.component.css']
})
export class AffectcategoryformodelComponent  {

  @Input()
  viewCreateModel: boolean ;
  @Input()
  viewAffectCategory: boolean ;
  @Input()
  categoriesSelected  : Categorie[]; 
  @Input()
  categoriesForCreateTask : Categorie[];

  @Output() 
  viewCreateModelTrue : EventEmitter<any> = new EventEmitter();
  @Output() 
  outputCategoriesSelected : EventEmitter<any> = new EventEmitter();
  @Output()
  viewCategorieFalse : EventEmitter<any> = new EventEmitter();

  categories : Categorie[]= [] ;
  taskName : string;
  taskDescription :string;
  categorieName: string;
  newCategorieName: string;

  constructor() { }

//gestion de l'ajout et retrait de taches
  addAnCategory(categorie: Categorie, index : number,taskSelected : any){
  //  console.log(taskSelected);
    this.categoriesSelected.push(categorie);
    this.categories.splice(index,1);
  //  console.log(categorie);

  
   
  }
  removeAllCategories(categories : Categorie[])
  {
    for ( let categorie of categories){
      this.categories.push(categorie);     
    }
    this.categoriesSelected.splice(0,this.categoriesSelected.length);
  
  }

  addAllCategories (categories : Categorie []){
 //   console.log(categories);
    for ( let categorie of categories){
      this.categoriesSelected.push(categorie);     
    }
    this.categories.splice(0,this.categories.length);
  }

  removeAnCategory(categorieselectionnee : Categorie,index: number){
 //  console.log(categorieselectionnee);
    this.categories.push(categorieselectionnee);
    this.categoriesSelected.splice(index,1);
  }

  createTask(taskName,taskDescription,categorieName,newCategorieName)
  {
    let createdTask = new Task();
    createdTask.name = taskName;
    createdTask.description = taskDescription;
    createdTask.newTask = true;   
    createdTask.userType="";
    
    if(categorieName)
    {
      createdTask.categorie = categorieName;
        for (let categorie of this.categoriesSelected )
        {
          if(categorie.name == categorieName)
          {
            categorie.task.push(createdTask);
         }
        };

    }    
    else
    {
      createdTask.categorie = newCategorieName;
      let createdCategorie = new Categorie();
      createdCategorie.name = newCategorieName;
      createdCategorie.newCategory = true;
      createdCategorie.task = [];
      createdCategorie.task.push(createdTask);
      this.categoriesSelected .push(createdCategorie);
      this.categoriesForCreateTask.push(createdCategorie);

    }
    this.taskName="";
    this.taskDescription="";
    this.categorieName="";
    this.newCategorieName="";
  
  }  


  

  onValid(){
  this.viewAffectCategory = false;
  this.viewCreateModel = true;
  this.viewCreateModelTrue.emit(this.viewCreateModel);
  this.viewCategorieFalse.emit(this.viewAffectCategory);
  this.outputCategoriesSelected.emit(this.categoriesSelected);
 // console.log(this.categoriesSelected);

  }
 

}
