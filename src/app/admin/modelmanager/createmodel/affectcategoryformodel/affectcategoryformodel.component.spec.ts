/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AffectcategoryformodelComponent } from './affectcategoryformodel.component';

describe('AffectcategoryformodelComponent', () => {
  let component: AffectcategoryformodelComponent;
  let fixture: ComponentFixture<AffectcategoryformodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectcategoryformodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectcategoryformodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
