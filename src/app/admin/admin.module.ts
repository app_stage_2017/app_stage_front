import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from'@angular/forms';

import {NavbarModule} from 'app/shared/navbar/navbar.module';
import {AppRoutingModule} from 'app/shared/app-routing/app-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { UsermanagerComponent } from './usermanager/usermanager.component';
import { ModelmanagerComponent } from './modelmanager/modelmanager.component';
import { CreatemodelComponent } from './modelmanager/createmodel/createmodel.component';
import { DeletemodelComponent} from './modelmanager/deletemodel/deletemodel.component';
import { CreateuserComponent } from './usermanager/createuser/createuser.component';
import { DeleteuserComponent } from './usermanager/deleteuser/deleteuser.component';

import {ModelService} from 'app/shared/service/model.service';
import {ClientService} from 'app/shared/service/client.service';
import { AffectcategoryformodelComponent } from './modelmanager/createmodel/affectcategoryformodel/affectcategoryformodel.component';
import { ProjectmanagerComponent } from './projectmanager/projectmanager.component';
import { ClientmanagerComponent } from './clientmanager/clientmanager.component';
import { CreateclientComponent } from './clientmanager/createclient/createclient.component';
import { DeleteclientComponent } from './clientmanager/deleteclient/deleteclient.component';

@NgModule({
  imports: [
    CommonModule,
    NavbarModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),

  ],
  declarations: [UsermanagerComponent, 
                ModelmanagerComponent, 
                CreatemodelComponent, 
                AffectcategoryformodelComponent, 
                ProjectmanagerComponent,
                DeletemodelComponent,
                CreateuserComponent,
                DeleteuserComponent,
                ClientmanagerComponent,
                CreateclientComponent,
                DeleteclientComponent ],
  providers: [ModelService,ClientService],
})
export class AdminModule { }
