import { Component, Input, Output, EventEmitter} from '@angular/core';
import {Client} from 'app/shared/model/client';
import {ClientService} from 'app/shared/service/client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-createclient',
  templateUrl: './createclient.component.html',
  styleUrls: ['./createclient.component.css']
})
export class CreateclientComponent {

@Input()
viewClientManager : boolean;
@Input()
viewCreateClient : boolean;

private client: Client;

  constructor(private clientservice : ClientService, private router : Router) { 
    this.client = new Client();
  }


public onCreate(value : Client){
 let newDate = new Date()  ;


 this.clientservice.createClient(value.name,value.code).subscribe();
  this.router.navigate(["/projectmanager"]); 

}

}
