import { Component, Input, Output, EventEmitter } from '@angular/core';
import {Client} from 'app/shared/model/client';
import {ClientService } from 'app/shared/service/client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-deleteclient',
  templateUrl: './deleteclient.component.html',
  styleUrls: ['./deleteclient.component.css']
})
export class DeleteclientComponent {
@Input()
viewClientManager : boolean;
@Input()
viewDeleteClient : boolean;
@Input()
clients :Client[];

@Output() 
viewDeleteClientFalse : EventEmitter<any> = new EventEmitter();
@Output() 
viewClientManagerTrue : EventEmitter<any> = new EventEmitter();

clientSelectName : string ;

  constructor(private clientservice : ClientService, private router : Router) { }

deleteClient(){
  let clientSelectId : number;
  for (let client of this.clients)
  {
    if(client.name == this.clientSelectName)
    {
      clientSelectId = client.id;
    }
  }
 // console.log(userSelectId);
  this.clientservice.deletClientFrompAPI(clientSelectId).subscribe();
  this.viewClientManager = true;
  this.viewDeleteClient = false;
  this.viewDeleteClientFalse.emit(this.viewDeleteClient);
  this.viewClientManagerTrue .emit(this.viewClientManager);
  this.router.navigate(["/projectmanager"]); 


}

}
