import { Component, OnInit } from '@angular/core';
import {Client} from 'app/shared/model/client';
import {ClientService } from 'app/shared/service/client.service';

@Component({
  selector: 'app-clientmanager',
  templateUrl: './clientmanager.component.html',
  styleUrls: ['./clientmanager.component.css']
})
export class ClientmanagerComponent implements OnInit {

  viewClientManager : boolean = true;
  viewCreateClient : boolean = false;
  viewDeleteClient : boolean = false;
  clients : Client [];

  constructor(private clientservice : ClientService) { }

  ngOnInit() {
    this.clientservice.getClientFromAPI().subscribe(
      clients => this.clients = clients,
      err => console.log(err.status)
    )
  }

  listenerviewDeleteClientFalse(event){
    this.viewDeleteClient =event;
  }

  listenerviewClientManagerTrue(event){
    this.viewClientManager =event;
  }  

createClient(){
  this.viewClientManager = false;
  this.viewCreateClient = true;

}

deleteClient(){
  this.viewClientManager = false;
  this.viewDeleteClient = true;
}

}
