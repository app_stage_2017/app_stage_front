import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientmanagerComponent } from './clientmanager.component';

describe('ClientmanagerComponent', () => {
  let component: ClientmanagerComponent;
  let fixture: ComponentFixture<ClientmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
