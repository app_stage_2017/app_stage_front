import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from'@angular/forms';
//import { ModalModule } from 'ng2-bootstrap/modal';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';

import {NavbarModule} from 'app/shared/navbar/navbar.module';

import {ProjectService} from 'app/shared/service/project.service';
import {LogService} from'app/shared/service/log.service';
import {ModelService} from 'app/shared/service/model.service';
import {FileService} from 'app/shared/service/file.service';
import {CreatePdfService} from 'app/shared/service/createpdf.service';
import {CurrentProjectsComponent} from './current-projects.component';

import {TacheFilterPipe} from'app/user/current-projects/tache-filter.pipe';
import {CategoryFilterPipe} from 'app/user/current-projects/category-filter.pipe';
import { FilterComponent } from './filter/filter.component';

import { Ng2CompleterModule } from "ng2-completer";
//import { ProgressbarModule } from 'ngx-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  //  NgbModule.forRoot(),
    HttpModule,
    NavbarModule,
    Ng2CompleterModule
  ],
  declarations: [
    CurrentProjectsComponent, 
  //  ModifCommentaireComponent,
    TacheFilterPipe,
    CategoryFilterPipe,
    FilterComponent,
     ],
  providers: [ProjectService, LogService,ModelService,FileService,CreatePdfService],
  exports: [CurrentProjectsComponent]
})
export class CurrentProjectsModule { }
