import {Pipe, PipeTransform} from '@angular/core';
//import {Tach} from 'app/data/project';
import {Task} from 'app/shared/model/project'

@Pipe({
   name: 'tacheFilter' 

})
export class TacheFilterPipe implements PipeTransform{

transform( value : Task[], searchTache: string =''){
            if (searchTache !== ''){
                let result = value.filter((tache : Task) => tache.name.toLocaleLowerCase().includes(searchTache));
            return result;
         }
         else {
             return value;
         }
        }
}