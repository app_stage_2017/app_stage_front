import { Component,Input, OnInit,Output,EventEmitter } from '@angular/core';
import {AffecModelService} from './affec-model.service';
import {Model} from 'app/shared/model/model';
import {Project} from 'app/shared/model/project';
import {User} from 'app/shared/model/users';
import {Categorie, Task} from 'app/shared/model/categorie';

import { CompleterService, CompleterData } from 'ng2-completer';
//import {TaskProjectService} from 'app/shared/taskproject.service';
//import {ProjectService} from 'app/shared/project.service';
//import {Router} from '@angular/router';



@Component({
  selector: 'num-rec-affectcategory',
  templateUrl: './affectcategory.component.html',
  styleUrls: ['./affectcategory.component.css']
 
})
export class AffectcategoryComponent implements OnInit{

  @Input()
  afficheaffect: boolean ;
  @Input()
  afficheCreaProj: boolean ;
  @Input()
  categoriesselectionnes : Categorie[];  
  @Input()
  categoriesForCreateTask : Categorie[];
  @Input()
  categorieNameService : CompleterData;

  @Output() 
  afficheCreaProjTrue : EventEmitter<any> = new EventEmitter();
  @Output() 
  afficheCategorieSelect : EventEmitter<any> = new EventEmitter();
  @Output()
  afficheViewCategorieFalse : EventEmitter<any> = new EventEmitter();

//  project : Project;
//  nbre: number;
  categories: Categorie[]=[];
  taskName : string;
  taskDescription : string;
  categorieName: string;
  newCategorieName : string;
//  categorieNameService : CompleterData;
  

//constructor(private _taskprojectservice: TaskProjectService, private _projectservice : ProjectService, private router : Router ) { }
constructor(private _completerService : CompleterService) { 
}

ngOnInit()
{
//  this.categorieNameService = this._completerService.local(this.categoriesForCreateTask,"name","name");
}
 
 
 


//gestion de l'ajout et retrait de taches
  addCategory(categorie: Categorie, index : number,taskSelected : any){
  //  console.log(taskSelected);
    this.categoriesselectionnes.push(categorie);
    this.categories.splice(index,1);
  //  console.log(categorie);

  
   
  }
  removeAllCategories(categories : Categorie[])
  {
    for ( let categorie of categories){
      this.categories.push(categorie);     
    }
    this.categoriesselectionnes.splice(0,this.categoriesselectionnes.length);
  
  }

  addAllCategories(categories : Categorie []){
 //   console.log(categories);
    for ( let categorie of categories){
      this.categoriesselectionnes.push(categorie);     
    }
    this.categories.splice(0,this.categories.length);
  }

  removeACategory(categorieselectionnee : Categorie,index: number){
 //  console.log(categorieselectionnee);
    this.categories.push(categorieselectionnee);
    this.categoriesselectionnes.splice(index,1);
  }

  createTask(taskName,taskDescription,categorieName,newCategorieName){
    let createdTask = new Task();
    createdTask.name = taskName;
    createdTask.description = taskDescription;   
    createdTask.userType="";
    
    if(categorieName)
    {
      createdTask.categorie = categorieName;
        for (let categorie of this.categoriesselectionnes)
        {
          if(categorie.name == categorieName)
          {
            categorie.task.push(createdTask);
         }
        };

    }    
    else
    {
      createdTask.categorie = newCategorieName;
      let createdCategorie = new Categorie();
      createdCategorie.name = newCategorieName;
      createdCategorie.task = [];
      createdCategorie.task.push(createdTask);
      this.categoriesselectionnes.push(createdCategorie);
      this.categoriesForCreateTask.push(createdCategorie);

    }
    this.taskName="";
    this.taskDescription="";
    this.categorieName="";
    this.newCategorieName="";
  }

onValid(){
  this.afficheaffect = false;
  this.afficheCreaProj = true;
  this.afficheCreaProjTrue.emit(this.afficheCreaProj);
  this.afficheViewCategorieFalse.emit(this.afficheaffect);
  this.afficheCategorieSelect.emit(this.categoriesselectionnes);
//  console.log(this.categoriesselectionnes);
  }

onCancel(){
  this.afficheaffect = false;
  this.afficheCreaProj = true;
  this.categoriesselectionnes = [];
  this.afficheCreaProjTrue.emit(this.afficheCreaProj);
  this.afficheViewCategorieFalse.emit(this.afficheaffect);
  this.afficheCategorieSelect.emit(this.categoriesselectionnes);
}

}
