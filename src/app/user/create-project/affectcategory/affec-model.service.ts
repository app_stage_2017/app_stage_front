import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Model} from 'app/shared/model/model';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import {ConnectServer} from'app/shared/service/connectserver.service';


@Injectable()
export class AffecModelService {
url : String;
  constructor(private _http: Http, private _connect : ConnectServer){
      this.url= this._connect.connect();
  }
//recupere les models sous format JSON a partir de l'API
  getModelFromAPI() : Observable <any>{
        return this._http.get(this.url + '/models')
               // .do(x=>console.log(x))
                .map(models => models.json());                       

   }

    getCategorieFromAPIByIdModel(idModel : number) : Observable <any> {
        return this._http
                    .get(this.url+"/categories/" + idModel)
                    .map (
                        categories => categories.json()
                    )
    } 
    

}
