/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AffecModelService } from './affec-model.service';

describe('AffecModelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AffecModelService]
    });
  });

  it('should ...', inject([AffecModelService], (service: AffecModelService) => {
    expect(service).toBeTruthy();
  }));
});
