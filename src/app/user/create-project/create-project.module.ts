import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from'@angular/forms';
import {CreateProjectComponent} from './create-project.component';



import {NavbarModule} from 'app/shared/navbar/navbar.module';
import { AffectcategoryComponent } from './affectcategory/affectcategory.component';
import {AppRoutingModule} from 'app/shared/app-routing/app-routing.module';
import {AffecModelService} from './affectcategory/affec-model.service';
import {TaskProjectService} from 'app/shared/service/taskproject.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { NgbPanelHeader } from './panel-header.directive';
import { Ng2CompleterModule } from "ng2-completer";

import {ClientService} from 'app/shared/service/client.service';
import {UserService} from 'app/shared/service/user.service';
import { AffectuserComponent } from './affectuser/affectuser.component';



@NgModule({
  imports: [
    CommonModule,
    NavbarModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    Ng2CompleterModule
    
  ],
  declarations: [
    CreateProjectComponent,
    AffectcategoryComponent,
 //   NgbPanelHeader,
    AffectuserComponent,
    
    
  ],
  exports : [],
  providers : [AffecModelService,TaskProjectService,ClientService, UserService]
})
export class CreateProjectModule { }
