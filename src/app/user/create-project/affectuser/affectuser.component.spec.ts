/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AffectuserComponent } from './affectuser.component';

describe('AffectuserComponent', () => {
  let component: AffectuserComponent;
  let fixture: ComponentFixture<AffectuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
