import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {User} from 'app/shared/model/users';
import {UserService} from 'app/shared/service/user.service';

@Component({
  selector: 'num-rec-affectuser',
  templateUrl: './affectuser.component.html',
  styleUrls: ['./affectuser.component.css']
})
export class AffectuserComponent  {
//listUsers : User [];
@Input()
afficheUser: boolean ;
@Input()
afficheCreaProj:boolean;
@Input()
listUsers: User [];
@Input()
userSelect : User [];

@Output() 
afficheCreaProjTrue : EventEmitter<any> = new EventEmitter();
@Output() 
afficheUserSelect : EventEmitter<any> = new EventEmitter();
@Output()
afficheUserFalse : EventEmitter<any> = new EventEmitter();

usersselectionnes : User[]=[];

constructor(private _userservice : UserService) { }

//gestion de l'ajout et retrait de taches
  addUser(user: User, index : number){
    this.usersselectionnes.push(user);
    this.listUsers.splice(index,1);
  //  console.log(categorie);

  
   
  }

  addAllUsers(users : User []){
    for ( let user of users){
      this.usersselectionnes.push(user);     
    }
    this.listUsers.splice(0,this.listUsers.length);
  }

  retirerAListe(userselectionne : User,index: number){
  // console.log(userselectionne);
    this.listUsers.push(userselectionne);
    this.usersselectionnes.splice(index,1);
  //  console.log(groupetache);
  }

  cancel(){
    this.afficheUser = false;
    this.afficheCreaProj = true;
    this.afficheCreaProjTrue.emit(this.afficheCreaProj);
    this.afficheUserFalse.emit(this.afficheUser);
  }

  affectuser(){
    this.afficheUser = false;
    this.afficheCreaProj = true;
    this.afficheCreaProjTrue.emit(this.afficheCreaProj);
    this.afficheUserFalse.emit(this.afficheUser);
  //  console.log(this.usersselectionnes);
    this.afficheUserSelect.emit(this.usersselectionnes);
  }

}
