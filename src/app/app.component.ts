import { Component } from '@angular/core';
import {CurrentProjectsComponent} from 'app/user/current-projects/current-projects.component';

@Component({
  selector: 'NumRec-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NumRec works!';
}
